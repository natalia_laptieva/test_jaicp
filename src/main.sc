require: slotfilling/slotFilling.sc
  module = sys.zb-common
require: localPatterns.sc

init:
   bind("postProcess", function($context){
       $context.session.lastState = $context.currentState;
       log(toPrettyString($context.session));
   });

theme: /
    state: Welcome
        # q!: *start
        q!: $hi *
        q!: $regex</start>
    
        script:
           $response.replies = $response.replies || [];
           $response.replies.push( {
               type: "image",
               imageUrl: "https://cdn.pixabay.com/photo/2017/06/05/11/01/airport-2373727_960_720.jpg",
               text: "Самолет"
           });
        random:
            # a: Здравствуйте123!
            # a: Приветствую!
            a: Меня зовут {{ capitalize($injector.botName) }}.
        go!: /Service/SuggestHelp
        
    state: CatchAll || noContext = true
        event!: noMatch
        a: Простите, я не поняла. Переформулируйте, пожалуйста, свой запрос.
        go!: {{ $session.lastState }}

theme: /Service
    state: SuggestHelp
        q: отмена || fromState = /Phone/Ask, onlyThisState = true
        a: Давайте я помогу вам купить билет на самолет, хорошо?

        buttons:
           "Да"
           "Нет"

        state: Accepted
           q: * (да/давай*/хорошо) *
           a: Отлично!
           go!: /Phone/Ask

        state: Rejected
           q: * (не/нет) *
           a: Боюсь, что ничего другого я пока предложить не могу...

theme: /Phone
    state: Ask || modal = true
        a: Для продолжения напишите, пожалуйста, мне ваш номер в формате 79000000000.

        buttons:
           "Отмена"

        state: Get
           q: $phone
           go!: /Phone/Confirm

        state: Wrong
           q: *
           a: Что-то не похоже на номер телефона...
           go!: /Phone/Ask

    state: Confirm
        # a: тест
        script:
           $temp.phone = $parseTree._phone || $client.phone;
        a: Ваш номер {{ $temp.phone }}, верно?

        script:
           $session.probablyPhone = $temp.phone;
        buttons:
           "Да"
           "Нет"

        state: Yes
            q: (да/верно)
            script:
               $client.phone = $session.probablyPhone;
               delete $session.probablyPhone;
            a: Хорошо. 
            go!: /Discount/Inform

        state: No
            q: (нет/не [верно])
            go!: /Phone/Ask
     
theme: /Discount
    state: Inform
        script:
           var nowDate = $jsapi.dateForZone("Europe/Moscow", "dd.MM");

           var answerText = "Хочу отметить, что вам крупно повезло! Сегодня (" + nowDate + ") действует акция!";

           var discount = "Купите билет сегодня и получите скидку в 10% на следующую покупку!";

           $reactions.answer(answerText);
           $reactions.answer(discount);

        # a: Акции на сегодня {{ $session.answerText }} {{ $session.discount }}. 
        # 
